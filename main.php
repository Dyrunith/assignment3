<?php
/**
 * Display Address book
 */
session_start();
require './library.php';

If (sesCheck($_SESSION['loggedIn']) == true) {
    ob_start();
    $data = [
        ['number' => 1, 'firstname' => 'Katherine', 'Lastname' => 'Ck', 'City' => 'Hamilton'],
        ['number' => 2, 'firstname' => 'John', 'Lastname' => 'Smith', 'City' => 'Burlington'],
        ['number' => 3, 'firstname' => 'Mark', 'Lastname' => 'Thomson', 'City' => 'Stoney Creek']
    ];
    include ('templates/table.php');
    $output = ob_get_clean();
} else {
    header('Location: index.php');
}

include 'templates/logout.html';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    if($_POST['logout'] == '1'){
        session_destroy();
        header('Location: index.php');
    }

} 

/** DO NOT CHANGE AFTER THIS LINE */
require 'templates/layout.php';